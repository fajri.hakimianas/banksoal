import axios from 'axios'
import store from '../store'
import {types} from "../store/type"
import {api} from "../api";

const token = window.localStorage.getItem('token')

const API = axios.create({
    baseURL: api,
    headers: {
        'Authorization': token ? `Bearer ${token}` : ''
    }
});

API.interceptors.request.use((req) => {
    const { auth } = store.getState();
    if(auth.token){
        req.headers.Authorization = `Bearer ${auth.token}`;
    }
    return req;
});

API.interceptors.response.use((res) => {
    return res;
}, (error) => {
    console.log(error.response);
    const { status } = error.response ? error.response.status : 500;

    if(status && status === 500){
        localStorage.clear();
        store.dispatch({
            type: types.LOGOUT_SUCCESS
        })
    }
    return Promise.reject(error)
});

export default API