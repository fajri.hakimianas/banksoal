import React, { useEffect } from 'react';
import './assets/style/style.css';
import "bootstrap/dist/css/bootstrap.min.css";
import "antd/dist/antd.css";

import Login from './page/Login';
import BahasaIndonesia from './page/soal/BahasaIndonesia';
import Matematika from './page/soal/Matematika';
import Homepage from './page/Homepage';
import Pelajaran from './page/admin/Pelajaran';

import {Switch, Route} from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import {isUserLoggedin} from './store/action';
import User from './page/admin/User';
import Inputsoal from './page/tutor/Inputsoal';
import Kimia from './page/soal/Kimia';


function App() {

  const dispatch = useDispatch();
  const auth = useSelector(state => state.auth)

  useEffect(() => {
    if(!auth.authenticate){
      dispatch(isUserLoggedin())
    }
  },[auth.authenticate, dispatch]);

  return (
    <div>
      <>
        <Switch>
          <Route exact path="/" component={Homepage} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/bahasa-indonesia" component={BahasaIndonesia} />
          <Route exact path="/matematika" component={Matematika} />
          <Route exact path="/biologi" component={Kimia} />

          <Route exact path="/pelajaran" component={Pelajaran} />
          <Route exact path="/register-user" component={User} />
          <Route exact path="/input-soal" component={Inputsoal} />
          {/* <Route exact path="/register" component={Register} /> */}
        </Switch>
      </>
    </div>
  )
}

export default App
