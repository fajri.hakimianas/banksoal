import React from 'react'
import { Link } from "react-router-dom"

import Header from '../component/Header'
import Banner from '../assets/images/Banner1.png'

import { Card, Avatar } from 'antd'
import { EditOutlined, EllipsisOutlined, SettingOutlined } from '@ant-design/icons'
import Footer from '../component/Footer'

const { Meta } = Card

function Homepage() {
    return (
        <>
            <Header/>
            <section className="container mt-3">
                <div className="row">
                    <div className="col-md-6">
                        <img src={Banner} alt="..." style={{ width: "auto", height: "auto" }}></img>
                    </div>
                    <div className="col-md-6">
                        <div className="kotak">
                            <h1 className="font-weight-bold">
                                Distributed assistance <br />
                                to their Educational Facilities
                            </h1>
                            <p className="mb-5 font-weight-light text-gray-500 w-75">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                                do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </p>
                        </div>
                    </div>
                </div>
            </section>

            <section>
                <h1 className=" h2 text-center font-weight-bold">
                    Please, Rise your hand for them!
                </h1>
                <p className="text-center font-weight-light text-gray-500 mb-5">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                    eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
                <div className="container">
                    <div className="row align-items-center ml-5">
                        <div className="col">
                            <Link to='/bahasa-indonesia'>
                                <Card
                                    style={{ width: 300, marginLeft:'50px' }}
                                    cover={
                                        <img
                                            alt="example"
                                            src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                                        />
                                    }
                                    actions={[
                                        <SettingOutlined key="setting" />,
                                        <EditOutlined key="edit" />,
                                        <EllipsisOutlined key="ellipsis" />,
                                    ]}
                                    >
                                    <Meta
                                        avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                                        title="Bahasa Indonesia"
                                        description="Good Luck!"
                                    />
                                </Card>
                            </Link>
                        </div>
                        <div className="col">
                            <Link to='/matematika'>
                                <Card
                                    style={{ width: 300, marginLeft:'50px' }}
                                    cover={
                                        <img
                                            alt="example"
                                            src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                                        />
                                    }
                                    actions={[
                                        <SettingOutlined key="setting" />,
                                        <EditOutlined key="edit" />,
                                        <EllipsisOutlined key="ellipsis" />,
                                    ]}
                                    >
                                    <Meta
                                        avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                                        title="Matematika"
                                        description="Good Luck!"
                                    />
                                </Card>
                            </Link>
                        </div>
                        <div className="col">
                            <Link to='/biologi'>
                                <Card
                                    style={{ width: 300, marginLeft:'50px' }}
                                    cover={
                                        <img
                                            alt="example"
                                            src="https://gw.alipayobjects.com/zos/rmsportal/JiqGstEfoWAOHiTxclqi.png"
                                        />
                                    }
                                    actions={[
                                        <SettingOutlined key="setting" />,
                                        <EditOutlined key="edit" />,
                                        <EllipsisOutlined key="ellipsis" />,
                                    ]}
                                    >
                                    <Meta
                                        avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                                        title="Biologi"
                                        description="Good Luck!"
                                    />
                                </Card>
                            </Link>
                        </div>
                    </div>
                </div>
            </section>
            <Footer/>
        </>
    )
}

export default Homepage
