import React, { useEffect } from 'react'
import { Checkbox, Row, Col } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { getSoal } from '../../store/action';

function Kimia() {

    const onChange = (checkedValues) => {
        console.log('checked = ', checkedValues);
    }

    const soal = useSelector(state => state.soal)
    console.log(soal)
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getSoal())
    }, [dispatch])

    return (
        <section className="container mt-5">
            <h4 className="text-center mb-5">
                Waktu Pengerjaan 10 Menit
            </h4>
            <div className="row">
                {
                    // eslint-disable-next-line array-callback-return
                    soal.listsoal.map((res) => {
                        console.log(res)
                        if(res.pelajaran === 'Kimia'){
                            return (
                                <div className="col-6">
                                    <div className="card" style={{width:'27rem'}}>
                                        <img src={res.images} className="card-img-top" alt="..."/>
                                        <div className="card-body">
                                            <p className="card-text">{res.soal}</p>
                                        </div>
                                    </div>
                                    <Checkbox.Group style={{ width: '100%' }} onChange={onChange}>
                                        <Row>
                                            <Col span={11}>
                                                <Checkbox value="A">{res.a}</Checkbox>
                                            </Col>
                                            <Col span={11}>
                                                <Checkbox value="B">{res.b}</Checkbox>
                                            </Col>
                                            <Col span={11}>
                                                <Checkbox value="C">{res.c}</Checkbox>
                                            </Col>
                                            <Col span={11}>
                                                <Checkbox value="D">{res.d}</Checkbox>
                                            </Col>
                                        </Row>
                                    </Checkbox.Group>
                                </div>
                            )
                        }
                    })
                }
            </div>
        </section>
    )
}

export default Kimia
