import React, { Component } from 'react';
import axios from 'axios';

export default class InputSoal extends Component {

    state = {
        fileImage: null
    }

    onImagesValidation = (element) => {
        const files = element.target.files
        console.log(files)
        try {
            if(element.target.files.length > 5) throw new Error("Select 5 Images Only")

            for(var i = 0; i < files.length; i++){
                if(files[i].size > 1000000) throw new Error('"' + files[i].name + '" More Than ' + Math.round(files[i].size / 1000000) + 'Mb')
            }

            this.setState({fileImage : files})
            
        } catch (error) {
            this.setState({imagesErrorMessage : error.message})
        }
    }

    handleSubmit = () => {

        var id_pelajaran = Number(this.id_pelajaran.value)
        var text_soal = this.text_soal.value
        var jawaban_a = this.jawaban_a.value
        var jawaban_b = this.jawaban_b.value
        var jawaban_c = this.jawaban_c.value
        var jawaban_d = this.jawaban_d.value

        // var data = {
        //     id_pelajaran,
        //     text_soal,
        //     jawaban_a,
        //     jawaban_b,
        //     jawaban_c,
        //     jawaban_d
        // }

        let fd = new FormData()
        // data = JSON.stringify(data)
        
        fd.append('id_pelajaran', id_pelajaran)
        fd.append('text_soal', text_soal)
        fd.append('jawaban_a', jawaban_a)
        fd.append('jawaban_b', jawaban_b)
        fd.append('jawaban_c', jawaban_c)
        fd.append('jawaban_d', jawaban_d)
        for(var i = 0 ; i < this.state.fileImage.length; i++){
            fd.append('image', this.state.fileImage[i])
        }

        axios.post('http://localhost:5000/input-soal', fd)
        .then(() => {
            alert('success')
            this.id_pelajaran.value = ''
            this.text_soal.value = ''
            this.jawaban_a.value = ''
            this.jawaban_b.value = ''
            this.jawaban_c.value = ''
            this.jawaban_d.value = ''
        })
        .catch((err) => {
            console.log(err)
        })
    }

    render() {
        return (
            <>
                <h1 className="jumbotron text-center bg-primary">
                    Input Soal
                </h1>
    
                <div className="container col-md-4 offset-md-4 pb-5">
                    <form>
                        <select
                            ref={(el) => this.id_pelajaran = el}
                            className="form-control mb-4 p-2"
                            id="exampleFormControlSelect1"
                            onChange={(e) => this.setState({id_pelajaran: e.target.value})}
                        >
                            <option value="1">Bahasa Indonesia</option>
                            <option value="2">Matematika</option>
                            <option value="3">Kimia</option>
                        </select>
                        <input
                            name="fileImage"
                            type="file"
                            className="form-control mb-4 p-2"
                            ref={(element) => this.file = element}
                            multiple="multiple"
                            accept="image/*"
                            onChange={this.onImagesValidation}
                        />
                        <input
                            name="text_soal"
                            placeholder="Pertanyaan"
                            type="text"
                            ref={(el) => this.text_soal = el}
                            className="form-control mb-4 p-2"
                            onChange={(e) => this.setState({text_soal: e.target.value})}
                        />
                        <input
                            name="jawaban_a"
                            placeholder="Pilihan Ganda A"
                            type="text"
                            ref={(el) => this.jawaban_a = el}
                            className="form-control mb-4 p-2"
                            onChange={(e) => this.setState({jawaban_a: e.target.value})}
                        />
                        <input
                            name="jawaban_b"
                            placeholder="Pilihan Ganda B"
                            type="text"
                            ref={(el) => this.jawaban_b = el}
                            className="form-control mb-4 p-2"
                            onChange={(e) => this.setState({jawaban_b: e.target.value})}
                        />
                        <input
                            name="jawaban_c"
                            placeholder="Pilihan Ganda C"
                            type="text"
                            ref={(el) => this.jawaban_c = el}
                            className="form-control mb-4 p-2"
                            onChange={(e) => this.setState({jawaban_c: e.target.value})}
                        />
                        <input
                            name="jawaban_d"
                            placeholder="Pilihan Ganda D"
                            type="text"
                            ref={(el) => this.jawaban_d = el}
                            className="form-control mb-4 p-2"
                            onChange={(e) => this.setState({jawaban_d: e.target.value})}
                        />        
                    </form>
                    <button
                        onClick={this.handleSubmit}
                        type="submit" 
                        className="btn btn-block btn-primary form-control" 
                    >
                        Submit
                    </button>
                </div>
            </>
        )
    }
}
