import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
// import {Link} from 'react-router-dom';

import {toast} from 'react-toastify';
import {SyncOutlined} from '@ant-design/icons';
import { getUser, inputUser } from '../../store/action';

function User() {
    
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [role, setRole] = useState("")

    const [loading, setLoading] = useState(false)

    const user = useSelector(state => state.user)
    console.log(user)

    const dispatch = useDispatch()

    useEffect(() => {
        setLoading(false)
        dispatch(getUser())
    },[dispatch])

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            setLoading(true)
            const form = {
                email,
                password,
                role
            }
            dispatch(inputUser(form))
            toast.success("Input User Success. Please Reload this page!");
            setLoading(false);
        } 
        catch (error) {
            toast(error.response)
            setLoading(false)
        }
    }

    return (
        <>
            <h4 className="jumbotron text-center bg-primary">
                Add Role User
            </h4>

            <div className="container col-md-4 offset-md-4 pb-5">
                <form onSubmit={handleSubmit}>
                    <input
                        required
                        type="email"
                        placeholder="Input Email"
                        className="form-control mb-4 p-2" 
                        value={email} 
                        onChange={(e) => setEmail(e.target.value)}
                    />
                    <input
                        required
                        type="text"
                        placeholder="Input Password"
                        className="form-control mb-4 p-2" 
                        value={password} 
                        onChange={(e) => setPassword(e.target.value)}
                    />
                    <input
                        required
                        type="text"
                        placeholder="Input Role Admin or User"
                        className="form-control mb-4 p-2" 
                        value={role} 
                        onChange={(e) => setRole(e.target.value)}
                    />
                    <button 
                        type="submit" 
                        className="btn btn-block btn-primary form-control" 
                        disabled={!email || !password || !role || loading}>
                            {loading ? <SyncOutlined spin/> : "Submit"}
                    </button>
                </form>


                <table className="table mt-5">
                    <thead>
                        <tr>
                        <th scope="col">No</th>
                        <th scope="col">Email</th>
                        <th scope="col">Password</th>
                        <th scope="col">Role</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            user.listuser.map(res => (
                                <tr>
                                    <th scope="row">{res.id}</th>
                                    <td>{res.email}</td>
                                    <td>{res.password}</td>
                                    <td>{res.role}</td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
            </div>
        </>
    )
}

export default User