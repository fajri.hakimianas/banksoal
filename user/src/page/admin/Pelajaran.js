import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
// import {Link} from 'react-router-dom';

import {toast} from 'react-toastify';
import {SyncOutlined} from '@ant-design/icons';
import { getPelajaran, inputPelajaran } from '../../store/action';

function Pelajaran() {
    
    const [nama_pelajaran, setNama_Pelajaran] = useState("")
    const [loading, setLoading] = useState(false)

    const pelajaran = useSelector(state => state.pelajaran)
    console.log(pelajaran)

    const dispatch = useDispatch()

    useEffect(() => {
        setLoading(false)
        dispatch(getPelajaran())
    },[dispatch])

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            setLoading(true)
            const form = {
                nama_pelajaran
            }
            dispatch(inputPelajaran(form))
            toast.success("Input Mata Pelajaran Success. Please Reload this page!");
            setLoading(false);
        } 
        catch (error) {
            toast(error.response)
            setLoading(false)
        }
    }

    return (
        <>
            <h4 className="jumbotron text-center bg-primary">
                Add Mata Pelajaran
            </h4>

            <div className="container col-md-4 offset-md-4 pb-5">
                <form onSubmit={handleSubmit}>
                    <input
                        required
                        type="text"
                        placeholder="Mata Pelajaran"
                        className="form-control mb-4 p-2" 
                        value={nama_pelajaran} 
                        onChange={(e) => setNama_Pelajaran(e.target.value)}
                    />
                    <button 
                        type="submit" 
                        className="btn btn-block btn-primary form-control" 
                        disabled={!nama_pelajaran || loading}>
                            {loading ? <SyncOutlined spin/> : "Submit"}
                    </button>
                </form>


                <table className="table mt-5">
                    <thead>
                        <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama Pelajaran</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            pelajaran.listdata.map(res => (
                                <tr>
                                    <th scope="row">{res.id}</th>
                                    <td>{res.nama_pelajaran}</td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
            </div>
        </>
    )
}

export default Pelajaran
