import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
import {Redirect} from 'react-router-dom';

// import {toast} from 'react-toastify';
import {SyncOutlined} from '@ant-design/icons';
import { login } from "../store/action";

const Login = () => {
    
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [loading, setLoading] = useState(false)

    const auth = useSelector(state => state.auth)
    const dispatch = useDispatch()

    useEffect(() => {
        setLoading(false)
    },[])

    const handleSubmit = async (e) => {
        e.preventDefault();

        const user = {
            email, password
        }

        dispatch(login(user))
    }

    if(auth.authenticate === true){
        return <Redirect to={'/'}/>
    }

    return (
        <>
            <h1 className="jumbotron text-center bg-primary">
                Login
            </h1>

            <div className="container col-md-4 offset-md-4 pb-5">
                <form onSubmit={handleSubmit}>
                    <input
                        required
                        type="email"
                        placeholder="Enter Email"
                        className="form-control mb-4 p-2" 
                        value={email} 
                        onChange={(e) => setEmail(e.target.value)}
                    />
                    <input
                        required
                        type="password"
                        placeholder="Enter Password"
                        className="form-control mb-4 p-2" 
                        value={password} 
                        onChange={(e) => setPassword(e.target.value)}
                    />
                    <button 
                        type="submit" 
                        className="btn btn-block btn-primary form-control" 
                        disabled={!email || !password || loading}>
                            {loading ? <SyncOutlined spin/> : "Submit"}
                    </button>
                </form>
            </div>
        </>
    )
}

export default Login;