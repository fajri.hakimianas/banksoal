import React, {useState, useEffect} from 'react';
import {Menu} from "antd";
import { LoginOutlined, AppstoreOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import { logout } from '../store/action';

const { Item } = Menu;

function Header() {
    
    const [current, setCurrent] = useState("")
    const [loading, setLoading] = useState(false)


    const dispatch = useDispatch();
    const auth = useSelector(state => state.auth)

    useEffect(() => {
        if(auth.authenticate){
            setLoading(false)
        }
    }, [auth.authenticate]);

    const Logout = () => {
        dispatch(logout())
    }

    const loggedinHeader = () => {
        if(auth.user[0].role === 'admin'){
            return (
                <>
                    <Menu mode="horizontal" selectedKeys={[current]}>
                        <Item key="/" icon={<AppstoreOutlined/>} onClick={(e) => setCurrent(e.key)}>
                            <a href="/">
                                Home
                            </a>
                        </Item>
                        <Item key="/register-user" icon={<AppstoreOutlined/>} onClick={(e) => setCurrent(e.key)}>
                            <a href="/register-user">
                                Data User
                            </a>
                        </Item>
                        {/* <Item key="/pelajaran" icon={<AppstoreOutlined/>} onClick={(e) => setCurrent(e.key)}>
                            <a href="/pelajaran">
                                Data Pelajaran
                            </a>
                        </Item> */}
                        <Item icon={<LoginOutlined/>} onClick={Logout} disabled={loading}>
                            <a href="/">
                               Logout
                            </a>
                        </Item>
                    </Menu>
                </>
            )
        }
        else {
            return (
                <>
                    <Menu mode="horizontal" selectedKeys={[current]}>
                        <Item key="/" icon={<AppstoreOutlined/>} onClick={(e) => setCurrent(e.key)}>
                            <a href="/">
                                Home
                            </a>
                        </Item>
                        <Item key="/input-soal" icon={<AppstoreOutlined/>} onClick={(e) => setCurrent(e.key)}>
                            <a href="/input-soal">
                                Management Soal
                            </a>
                        </Item>
                        <Item icon={<LoginOutlined/>} onClick={Logout}>
                            <a href="/">
                               Logout
                            </a>
                        </Item>
                    </Menu>
                </>
            )
        }
    }

    const logoutHeader = () => {
        return (
            <>
                <Menu mode="horizontal" selectedKeys={[current]}>
                    <Item key="/" icon={<AppstoreOutlined/>} onClick={(e) => setCurrent(e.key)}>
                        <a href="/">
                            Home
                        </a>
                    </Item>
                    <Item key="/login" icon={<LoginOutlined/>} onClick={(e) => setCurrent(e.key)}>
                        <a href="/login">
                           Login
                        </a>
                    </Item>
                </Menu>
            </>
        )
    }

    return (
        <div>
            {
                auth.authenticate ? loggedinHeader() : logoutHeader()
            }
        </div>
    )

}

export default Header;