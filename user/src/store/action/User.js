import { types } from "../type";
import API from '../../axios';

export const getUser = () => {
    return async dispatch => {
        dispatch({
            type: types.GET_USER_REQUEST
        })

        const res = await API.get(`/get-data-user`);

        if(res.status === 200) {
            const {data} = res.data
            dispatch({
                type: types.GET_USER_SUCCESS,
                payload: {
                    listuser: data
                }
            })
        }
    }
}

export const inputUser = (form) => {
    console.log(form)
    return async (dispatch) => {
        dispatch({ 
            type: types.INPUT_USER_REQUEST
        });

        const res = await API.post(`/register-user`, form);

        if(res.status === 201){
            dispatch({
                type: types.INPUT_USER_SUCCESS
            });
        } 
        else {
            dispatch({ type: types.INPUT_USER_FAILED});
        }
    
    }
}