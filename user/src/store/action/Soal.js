import { types } from "../type";
import API from '../../axios';

export const getSoal = () => {
    return async dispatch => {
        dispatch({
            type: types.GET_SOAL_REQUEST
        })

        const res = await API.get(`/get-data-soal`);

        if(res.status === 200) {
            const {data} = res.data
            dispatch({
                type: types.GET_SOAL_SUCCESS,
                payload: {
                    listsoal: data
                }
            })
        }
        else {
            dispatch({
                type: types.GET_SOAL_FAILED,
                error: res.data.error
            })
        }
    }
}

export const inputSoal = (form) => {
    console.log(form)
    return async (dispatch) => {
        dispatch({ 
            type: types.INPUT_SOAL_REQUEST
        });

        const res = await API.post(`/input-soal`, form);

        if(res.status === 201){
            dispatch({
                type: types.INPUT_SOAL_SUCCESS
            });
        } 
        else {
            dispatch({ type: types.INPUT_SOAL_FAILED});
        }
    
    }
}