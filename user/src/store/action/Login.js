import { types } from "../type";
import API from '../../axios';

export const login = (user) => {
    console.log(user)
    return async (dispatch) => {
        dispatch({ 
            type: types.LOGIN_REQUEST 
        });

        const res = await API.post(`/login`, {
            ...user
        });

        if(res.status === 200) {
            const {token, user} = res.data;
            // Simpan Token ke localStorage
            localStorage.setItem('token', token);
            localStorage.setItem('user', JSON.stringify(user));
            dispatch({
                type: types.LOGIN_SUCCESS,
                payload: {
                    token,
                    user
                }
            })
        }
        else {
            if(res.status === 400) {
                dispatch({
                    type: types.LOGIN_FAILED,
                    payload: {
                        error: res.data.error
                    }
                })
            }
        }
    }
}

export const isUserLoggedin = () => {
    return async dispatch => {
        const token = localStorage.getItem('token');
        if(token) {
            const user = JSON.parse(localStorage.getItem('user'));
            dispatch({
                type: types.LOGIN_SUCCESS,
                payload: {
                    token,
                    user
                }
            })
        }
        else {
            dispatch({
                type: types.LOGIN_FAILED,
                payload: {
                    error: 'Failed to Login'
                }
            })
        }
    }
}

export const logout = () => {
    return async dispatch => {
        dispatch({
            type: types.LOGOUT_REQUEST
        });

        const res = await API.post(`/logout`);
        if(res.status === 200) {
            localStorage.clear();
            dispatch({
                type: types.LOGOUT_SUCCESS
            })
        }
        else {
            dispatch({
                type: types.LOGOUT_FAILED,
                payload: {
                    error: res.data.error
                }
            });
        }
    }
}