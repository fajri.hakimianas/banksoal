import { types } from "../type";
import API from '../../axios';

export const getPelajaran = () => {
    return async dispatch => {
        dispatch({
            type: types.GET_PELAJARAN_REQUEST
        })

        const res = await API.get(`/get-data-pelajaran`);

        if(res.status === 200) {
            const {data} = res.data
            dispatch({
                type: types.GET_PELAJARAN_SUCCESS,
                payload: {
                    listdata: data
                }
            })
        }
    }
}

export const inputPelajaran = (form) => {
    console.log(form)
    return async (dispatch) => {
        dispatch({ 
            type: types.INPUT_PELAJARAN_REQUEST
        });

        const res = await API.post(`/input-pelajaran`, form);

        if(res.status === 201){
            dispatch({
                type: types.INPUT_PELAJARAN_SUCCESS
            });
        } 
        else {
            dispatch({ type: types.INPUT_PELAJARAN_FAILED});
        }
    
    }
}