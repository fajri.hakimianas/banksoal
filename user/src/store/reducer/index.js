import Login from "./Login"
import Pelajaran from "./Pelajaran"
import Soal from "./Soal"
import User from "./User"

import { combineReducers } from 'redux';

const rootReducers = combineReducers({
    auth: Login,
    pelajaran: Pelajaran,
    soal: Soal,
    user: User
});

export default rootReducers