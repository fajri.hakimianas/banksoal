/* eslint-disable import/no-anonymous-default-export */
/* eslint-disable default-case */

import { types } from '../type'

const initialState = {
    listuser: [],
    error: null,
    loading: false
}

export default function User(state = initialState, action){

    console.log(action)

    const { type, payload } = action;
    switch(type){
        case types.GET_USER_REQUEST:
            state = {
                ...state,
                loading: true,
                error: null
            }
            break;
        case types.GET_USER_SUCCESS:
            state = {
                ...state,
                listuser: payload.listuser
            }
            break;
        case types.GET_USER_FAILED:
            state = {
                ...state,
                loading: false
            }
            break;
    }
    return state;
}