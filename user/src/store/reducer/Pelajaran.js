/* eslint-disable import/no-anonymous-default-export */
/* eslint-disable default-case */

import { types } from '../type'

const initialState = {
    listdata: [],
    error: null,
    loading: false
}

export default function Pelajaran(state = initialState, action){

    console.log(action)

    const { type, payload } = action;
    switch(type){
        case types.GET_PELAJARAN_REQUEST:
            state = {
                ...state,
                loading: true,
                error: null
            }
            break;
        case types.GET_PELAJARAN_SUCCESS:
            state = {
                ...state,
                listdata: payload.listdata
            }
            break;
        case types.GET_PELAJARAN_FAILED:
            state = {
                ...state,
                loading: false
            }
            break;
    }
    return state;
}