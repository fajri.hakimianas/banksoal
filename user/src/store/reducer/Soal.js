/* eslint-disable import/no-anonymous-default-export */
/* eslint-disable default-case */

import { types } from '../type'

const initialState = {
    listsoal: [],
    error: null,
    loading: false
}

export default function Soal(state = initialState, action){

    console.log(action)

    const { type, payload } = action;
    switch(type){
        case types.GET_SOAL_REQUEST:
            state = {
                ...state,
                loading: true,
                error: null
            }
            break;
        case types.GET_SOAL_SUCCESS:
            state = {
                ...state,
                listsoal: payload.listsoal
            }
            break;
        case types.GET_SOAL_FAILED:
            state = {
                ...state,
                loading: false
            }
            break;
    }
    return state;
}