/* eslint-disable import/no-anonymous-default-export */
/* eslint-disable default-case */

import { types } from '../type'

const initialState = {
    token: null,
    user: {
        email: ''
    },
    authenticate: false,
    authenticating: false,
    loading: false,
    error: null,
    message: ''
}

export default function Login(state = initialState, action){

    console.log(action)

    const { type, payload } = action;
    switch(type){
        case types.LOGIN_REQUEST:
            state = {
                ...state,
                authenticating: true
            }
            break;
        case types.LOGIN_SUCCESS:
            state = {
                ...state,
                user: payload.user,
                token: payload.token,
                authenticate: true,
                authenticating: false
            }
            break;
        case types.LOGOUT_REQUEST:
            state = {
                ...state,
                loading: true
            }
            break;
        case types.LOGOUT_SUCCESS:
            state = {
                ...initialState
            }
            break;
        case types.LOGOUT_FAILED:
            state = {
                ...state,
                error: payload.error,
                loading: false
            }
            break;
    }
    return state
}