const jwt = require('jsonwebtoken');
const jwtConfig = require('../config/jwt');

exports.Encode = (data) => {
    return jwt.sign(data, jwtConfig.JWT_UNIQUE, {
        expiresIn: '2d'
    });
};

exports.Decode = (token) => {
    return jwt.verify(token, jwtConfig.JWT_UNIQUE);
};