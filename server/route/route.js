const express = require('express');
const router = express.Router();

const admin = require('../controllers/admin');
const login = require('../controllers/login');
const logout = require('../controllers/logout');
const soal = require('../controllers/soal');
const pelajaran = require('../controllers/pelajaran');
const image = require('../controllers/image');

const verifikasi = require('../middleware');

// LOGIN
router.post('/login', login.loginUser);

// LOGOUT
router.post('/logout', logout.userLogout);

// ADMIN
router.post('/register-user', verifikasi('admin'), admin.registerUser);
router.get('/get-data-user', verifikasi('admin'), admin.getDataUser);
router.post('/input-pelajaran', verifikasi('admin'), admin.inputMatpel);
// router.post('/google-login', admin.googlelogin);

// TUTOR
router.post('/input-soal', soal.insertSoal);
router.patch('/edit-soal/:id', soal.updateSoal);

router.patch('/edit-image/:id', image.updateImage);

// ADMIN DAN TUTOR
router.get('/get-data-soal', soal.getDataSoal);
router.get('/get-data-pelajaran', pelajaran.getDataPelajaran);


module.exports = router;