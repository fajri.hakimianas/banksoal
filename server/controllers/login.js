const connection = require('../config/database');
const bcrypt = require('../lib/bcrypt');
const jwt = require('../lib/jwt');

// const {OAuth2Client} = require('google-auth-library');
// const client = new OAuth2Client("789892855685-d7g7ates4e134afntrq31734rmobnb2h.apps.googleusercontent.com");

// LOGIN TUTOR
// exports.tutorLogin = function(req, res){

//     var email = req.body.email;
//     var password = req.body.password;
//     const encryptPassword = bcrypt.Encrypt(password);

//     var sql = 'SELECT * FROM tutor WHERE email = ? AND password = ?';
//     connection.query(sql, [email, encryptPassword], function(err, user){
//         if(err) throw err;
        
//         var token = jwt.Encode({
//             user: user.id
//         });

//         if(user.length > 0){
//             res.status(200).send({
//                 message: "Login Successfully!",
//                 token,
//                 user
//             })
//         }
//         else if(!user.email || !user.password) {
//             res.status(400).send({
//                 message: "Your Email Address or Password is wrong!"
//             })
//         }
//     })
// }

// LOGIN
exports.loginUser = function(req, res){

    var email = req.body.email;
    var password = req.body.password;
    const encryptPassword = bcrypt.Encrypt(password);

    var sql = 'SELECT * FROM user WHERE email = ? AND password = ?';
    connection.query(sql, [email, encryptPassword], function(err, user){
        if(err) throw err;
        
        let role = user[0].role;

        var token = jwt.Encode({
            user: user.id
        });

        if(user.length > 0 || role === 'admin' && role === 'tutor'){
            res.status(200).send({
                message: "Login Successfully!",
                token,
                user
            })
        }
        else if(!user.email || !user.password) {
            res.status(400).send({
                message: "Your Email Address or Password is wrong!"
            })
        }
    })
}