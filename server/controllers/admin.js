const connection = require('../config/database');
const bcrypt = require('../lib/bcrypt');
const jwt = require('../lib/jwt');
// const {OAuth2Client} = require('google-auth-library');
// const client = new OAuth2Client("789892855685-d7g7ates4e134afntrq31734rmobnb2h.apps.googleusercontent.com");

// REGISTER USER
exports.registerUser = function(req, res) {

    const password = req.body.password;
    const encryptPassword = bcrypt.Encrypt(password);

    register = {
        email: req.body.email,
        password: encryptPassword,
        role: req.body.role
    }
    // Cek double email dan password
    var sql = 'SELECT * FROM user WHERE email = ?';
    connection.query(sql, [register.email], function(err, data){
        if(err) throw err

        if(data.length > 0){
            res.send({
                message: "Email was already exist!"
            });
        }
        else {
            // Insert User ke database User
            var sql = 'INSERT INTO user SET ?';
            connection.query(sql, register, function(err){
                if(err) throw err;

                res.status(201).json({
                    message: "Register User Success!"
                });
            });
        }
    })
}

exports.getDataUser = function(req, res){
    var sql = "SELECT * FROM user";
    connection.query(sql, (err, data) => {
        if(err) throw err;
        res.status(200).json({
            data
        })
    })
}


// INPUT MATA PELAJARAN
exports.inputMatpel = function(req, res){

    inputMatpel = {
        nama_pelajaran: req.body.nama_pelajaran
    }

    // Cek double nama mata pelajaran
    var sql = 'SELECT * FROM pelajaran WHERE nama_pelajaran = ?';
    connection.query(sql, [inputMatpel.nama_pelajaran], function(err, data){
        if(err) throw err

        if(data.length > 0){
            res.send({
                message: "Nama Mata Pelajaran was already exist!"
            });
        }
        else {
            // Insert Mata Pelajaran ke Tabel Pelajaran
            var sql = 'INSERT INTO pelajaran SET ?';
            connection.query(sql, inputMatpel, function(err){
                if(err) throw err
            });
            res.status(201).send({
                message: "Input Mata Pelajaran Success!"
            });
        }
    })
}

// GOOGLE LOGIN
// exports.googlelogin = (req, res) => {

//     const {tokenId} = req.body;

//     client.verifyIdToken({
//         idToken: tokenId, 
//         audience: "789892855685-d7g7ates4e134afntrq31734rmobnb2h.apps.googleusercontent.com"
//     }).then(response => {
//         const {email_verified, name, email} = response.payload;

//         if(email_verified){
//             var sql = 'SELECT * FROM admin WHERE email = ?';
//             connection.query(sql, [email], function(err, result){
//                 console.log(result)
//                 if(err) throw err;
        
//                 if(result.email || result.role === 'admin'){
//                     res.json({
//                         message: "Login Successfully!",
//                     })
//                 }
//                 else if(!result.email) {
//                     res.send({
//                         message: "Your Email Address is wrong!"
//                     })
//                 }
//             })
//         }

//         console.log(response.payload)
//     })
// }