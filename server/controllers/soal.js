const connection = require('../config/database');
const singleUpload = require("../middleware/SingleUpload")();
// const multipleUpload = require("../middleware/MultipleUpload")();

// INPUT SOAL Single Image
exports.insertSoal = function(req,res){
    // Upload Image to API
    singleUpload(req, res, (err) => {
        try {
            if(err) throw err

            console.log(req.file)
            // Request Validation Filter
            if(req.filteringValidation) throw { message : req.filteringValidation }
            if(req.file === undefined) throw { message : 'File Not Found'}
            
            // Get Image Path
            var imagePath = 'http://localhost:5000/' + req.file.path

            console.log(imagePath);

            var data = {
                id_pelajaran: req.body.id_pelajaran,
                text_soal: req.body.text_soal,
                jawaban_a: req.body.jawaban_a,
                jawaban_b: req.body.jawaban_b,
                jawaban_c: req.body.jawaban_c,
                jawaban_d: req.body.jawaban_d,
            }
            console.log(data)

            // try {
            //     var dataParsed = JSON.parse(data)
            //     console.log(dataParsed)
            // } catch (error) {
            //     console.log(error)
            // }

            // INSERT DATA SOAL FROM TUTOR KE TABEL SOAL
            var insertSoal = "INSERT INTO soal SET ?";
            connection.query(insertSoal, [data], (err, result) => {
                if(err) throw err;

                let soal_id = result.insertId

                // SEND SOAL ID DAN IMAGE PATH KE TABEL IMAGE
                var sql = "INSERT INTO image SET ?";
                connection.query(sql, {
                    soal_id,
                    image: imagePath
                }, (err) => {

                    try {
                        if(err) throw err;
    
                        res.send({
                        message: "Input Data Soal Success"
                        })
                    }
                    catch(error) {
                        res.send({
                            message: "Input Data Soal Failed!"
                        })
                    }
                });
            })
        }
        catch (error) {
            console.log(error)
            res.status(404).send({
                error : true, 
                message : error.message
            })
        }
    })
}

// Get Data Soal Per Masing-masing Mata Pelajaran
exports.getDataSoal = function(req, res){ 
    
    var sql="SELECT soal.id, pelajaran.nama_pelajaran, image.image, soal.text_soal, soal.jawaban_a, soal.jawaban_b, soal.jawaban_c, soal.jawaban_d FROM soal JOIN image ON image.soal_id = soal.id JOIN pelajaran ON pelajaran.id = soal.id_pelajaran";
    connection.query(sql, (err, result) => {
        if(err) throw err;

        let dataTransform = [];
        result.forEach(function(value){
            
            let idSoal = null

            // cek apakah sudah ada ID Soal didalam data Transform
            dataTransform.forEach((find, index) => {
                if(find.id === value.id){
                    idSoal = index
                }
            });
            
            dataTransform.push({
                id: value.id,
                pelajaran: value.nama_pelajaran,
                name: value.name,
                images: value.image,
                soal: value.text_soal,
                a: value.jawaban_a,
                b: value.jawaban_b,
                c: value.jawaban_c,
                d: value.jawaban_d,
            })
        })

        res.send({
            data: dataTransform
        })   
    });
}

// UPDATE SOAL
exports.updateSoal = function(req,res){
    const idSoal = req.params.id
    // Upload Image to API
    singleUpload(req, res, (err) => {
        try {
            if(err) throw err
            console.log(req.body.data);

            // STEP 2 Request Validation Filter
            if(req.filteringValidation) throw { message : req.filteringValidation }
            if(req.file === undefined) throw { message : 'File Not Found'}

            // STEP 3 Get Image Path
            // Get data input from user
            var editData = req.body.data;
            editData = JSON.parse(editData)

            console.log(idSoal)

            // GET new IMAGE PATH untuk di delete apabila error di tengah jalan
            let newImage = req.file.map((value) => {
                return value.path
            })

            // SELECT DATA ID SOAL dari TABEL SOAL
            var soalID = "SELECT * FROM soal WHERE id = ?";
            connection.query(soalID, idSoal, (err, result) => {
                console.log(result)

                if(result === 0){
                    deleteImage(newImage, req, res)
                    return res.json({
                        error: true,
                        message: "ID SOAL Not Found!"
                    })
                }

                var update = "UPDATE soal SET ? WHERE id = ?";
                connection.query(update, [editData, idSoal], (err, result) => {

                    if(err){
                        deleteImage(newImage, req, res)
                        return connection.rollback(() => {
                            throw err
                        })
                    }

                    // GET PATH IMAGE yg LAMA untuk menghapusnya di STORAGE (API)
                    var pathImage = "SELECT image from image WHERE soal_id = ?";
                    connection.query(pathImage, idSoal, (err, result) => {
                        if(err){
                            deleteImage(newImage, req, res)
                            return connection.rollback(() => {
                                throw err
                            })
                        }
                        let pathImageLama = result.map((value) => {
                            return value.image.replace('http://localhost:5000/', '')
                        })

                        // Delete Old Images Path dari TABEL IMAGE
                        var deletePathImage = "DELETE FROM image WHERE soal_id";
                        connection.query(deletePathImage, idCampaign, (err, result) => {
                            if(err){
                                deleteImage(newImage, req, res)
                                return connection.rollback(() => {
                                    throw err
                                })
                            }

                            // GET NEW IMAGE PATH UNTUK INSERT ke TABEL IMAGE
                            let newPathImage = req.file.map((value) => {
                                return [
                                    idCampaign,
                                    'http://localhost:5000/' + value.path
                                ]
                            })

                            // INSERT ID SOAL DAN NEW IMAGE PATH ke TABEL IMAGE
                            connection.query("INSERT INTO image (soal_id, image) VALUES ?", [newPathImage], (err, result) => {
                                if(err){
                                    deleteImage(newImage, req, res)
                                    return connection.rollback(() => {
                                        throw err
                                    })
                                }

                                // HAPUS IMAGE LAMA dari STORAGE (API)
                                deleteImage(pathImageLama, req, res)

                                connection.commit((err) => {
                                    if(err){
                                        deleteImage(newImage, req, res)
                                        return connection.rollback(() => {
                                            throw err
                                        })
                                    }

                                    res.json({
                                        error: false,
                                        message: "Update Product Success"
                                    })
                                })
                            })
                        })
                    })
                });
            });
        }
        catch (error) {
            res.status(404).send({
                error : true, 
                message : error.message
            })
        }
    })
}

// DELETE SOAL
exports.deleteSoal = function(req, res){
    const idSoal = req.params.idSoal

    try {
        if(!idSoal) throw { message : 'Id Soal Cannot Null' }

        connection.beginTransaction((err) => {
            if(err) throw err

            connection.query('SELECT * FROM soal WHERE id = ?;', idSoal, (err, result) => {
                if(err){
                    return connection.rollback(() => {
                        throw err
                    })
                }

                if(result.length === 0){
                    return res.json({
                        error : true,
                        message : 'Id Soal Not Found'
                    })
                }

                // Step1. Delete SOAL on "Table SOAL"
                connection.query('DELETE from soal WHERE id = ?;', idSoal, (err, result) => {
                    if(err){
                        deleteImages(req.files.map((value) => value.path), req, res)
                        return connection.rollback(() => {
                            throw err
                        })
                    }
    
                    // Step2. Get Path Images yang Lama ---> Untuk Menghapus Images Lama di Storage(API)
                    connection.query('SELECT image from image WHERE soal_id = ?;', idSoal, (err, result) => {
                        if(err){
                            deleteImages(req.files.map((value) => value.path), req, res)
                            return connection.rollback(() => {
                                throw err
                            })
                        }
                        
                        // Step3. Result Dari Query Step2. Yaitu :
                        // result = [
                        //     {image : "path1"},
                        //     {image : "path2"},
                        //     {image : "path3"},
                        // ]
                        
                        //        Akan Diubah Menjadi :
                        // result = ['path1','path2','path3']

                        let oldImagePath = result.map((value) => {
                            return value.image.replace('http://localhost:5000/', '')
                        })
    
                        // Step4. Delete Images on "Table Images"
                        connection.query('DELETE FROM image WHERE soal_id = ?;', idSoal, (err, result) => {
                            if(err){
                                deleteImages(req.files.map((value) => value.path), req, res)
                                return connection.rollback(() => {
                                    throw err
                                })
                            }
    
                            // Step5. Delete Old Images on Storage (API)
                            deleteImages(oldImagePath, req, res)
    
                            connection.commit((err) => {
                                if(err){
                                    deleteImages(req.files.map((value) => value.path), req, res)
                                    return connection.rollback(() => {
                                        throw err
                                    })
                                }
    
                                res.json({
                                    error : false,
                                    message : 'Delete Soal Success'
                                })
                            })
                        })
                    })
                })
            })
        })
    } catch (error) {
        res.json({
            error : true,
            message : error.message
        })
    }
}

// INPUT SOAL Multiple Image
// exports.insertSoal = function(req,res){
//     // Upload Image to API
//     multipleUpload(req, res, (err) => {
//         try {
//             if(err) throw err

//             console.log(req.file)

//             // Request Validation Filter
//             if(req.filteringValidation) throw { message : req.filteringValidation }
//             if(req.files.length === 0) throw { message : 'File Not Found'}
            
//             // Get Image Path
//             var imagePath = 'http://localhost:5000/' + req.file.path

//             console.log(imagePath)
//             var data = req.body.data
//             console.log(data)

//             try {
//                 var dataParsed = JSON.parse(data)
//                 console.log(dataParsed)
//             } catch (error) {
//                 console.log(error)
//             }
                
//             // INPUT DATA SOAL FROM TUTOR KE TABEL SOAL
//             var sql = "INSERT INTO soal SET ?";
//             connection.query(sql, {
//                 gambar: imagePath,
//                 nama_pelajaran: dataParsed.nama_pelajaran,
//                 text_soal: dataParsed.text_soal,
//                 jawaban_a: dataParsed.jawaban_a,
//                 jawaban_b: dataParsed.jawaban_b,
//                 jawaban_c: dataParsed.jawaban_c,
//                 jawaban_d: dataParsed.jawaban_d,
//             }, (err, result) => {
//                 console.log(result)
//                 try {
//                     if(err) throw err;

//                     res.send({
//                     message: "Input Soal Success"
//                     })
//                 }
//                 catch(error) {
//                     res.send({
//                         message: "Input Soal Failed!"
//                     })
//                 }
//             });

//         }
//         catch (error) {
//             console.log(error)
//             res.status(404).send({
//                 error : true, 
//                 message : error.message
//             })
//         }
//     })
// }