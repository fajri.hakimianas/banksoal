const connection = require('../config/database');
const singleUpload = require("../middleware/SingleUpload")();

exports.updateImage = function(req,res){
    const idImage = req.params.idImage
    console.log(req.filename)
    // Upload Image to API
    singleUpload(req, res, (err) => {
        try {
            if(err) throw err
            console.log(req.body.data);

            // STEP 2 Request Validation Filter
            if(req.filteringValidation) throw { message : req.filteringValidation }
            if(req.filename === undefined) throw { message : 'File Not Found'}

            // GET new IMAGE PATH untuk di delete apabila error di tengah jalan
            let newImage = req.filename.map((value) => {
                return value.path
            })

            connection.beginTransaction((err) => {
                // SELECT DATA ID SOAL dari TABEL SOAL
                var imageId = "SELECT * FROM image WHERE id = ?";
                connection.query(imageId, idImage, (err, result) => {
                    console.log(result)

                    if(err){
                        deleteImages(newImage, req, res)
                        return connection.rollback(() => {
                            throw err
                        })
                    }
                    if(result === 0){
                        deleteImage(newImage, req, res)
                        return res.json({
                            error: true,
                            message: "ID SOAL Not Found!"
                        })
                    }

                    let oldImagePath = [result[0].image.replace('http://localhost:5000/', '')]

                    // Step4. Get New Image Path to Insert
                    let newImagePath = 'http://localhost:5000/' + req.file.path

                    var update = "UPDATE image SET ? WHERE id = ?";
                    connection.query(update, [{image: newImagePath}, idImage], (err, result) => {

                        if(err){
                            deleteImages(newImage, req, res)
                            return connection.rollback(() => {
                                throw err
                            })
                        }

                        // Step6. Delete Old Image on Storage (API)
                        deleteImages(oldImagePath, req, res)

                        connection.commit((err) => {
                            if(err){
                                deleteImages(newImage, req, res)
                                return connection.rollback(() => {
                                    throw err
                                })
                            }
                            res.json({
                                error : false,
                                message : "Update Image Success"
                            })
                        })
                    });
                });
            })
        }
        catch (error) {
            res.status(404).send({
                error : true, 
                message : error.message
            })
        }
    })
}