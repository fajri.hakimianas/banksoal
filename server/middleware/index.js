// const connection = require('../config/database');
const jwt = require('jsonwebtoken');

function verifikasi(role){
    return function(req, res, next){
        //cek authorizzation header
        var tokenWithBearer = req.headers.authorization;

        if(tokenWithBearer){
            var token = tokenWithBearer.split(' ')[1];

            // verifikasi
            jwt.verify(token, 'ajslfneiekmf44092dsfhsrarejyjyukjdtjsryralakoenmklpwlokendggvgahjkle', function(err, decoded){
                if(err){
                    return res.status(401).send({
                        message:'Token tidak terdaftar!'
                    });
                }
                else {
                    if(role === 'admin'){
                        req.auth = decoded;
                        next();
                    }
                    else {
                        return res.status(401).send({
                            message:'Gagal meng-otorisasi role anda!'
                        });
                    }
                }
            })
        }
        else {
            return res.status(401).send({
                message:'Token tidak tersedia!'
            });
        }
    }
}

module.exports = verifikasi;