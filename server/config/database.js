const mysql = require("mysql2");
require('dotenv').config();
const connection = mysql.createPool({
  connectionLimit: 100,
  host: "localhost",
  user: "root",
  password: "",
  database: "bank_soal",
  port: "3306",
});

module.exports = connection;